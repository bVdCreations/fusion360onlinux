# possible resources
document to hold all the resources gathered in the explore phase 
date explore phase : 9 jan 2019

## wine resoucres

 - [wine link fusion 360][1]&rarr; interesting guide
 - [second wine link fusion 360][10] &rarr; most recend guide to install

## fusion 360 resources

 - [fusion 360 product page][2] &rarr; page to download .exe file (on a windows machine)
 - ~~[forum fusion 360][4]~~ &rarr; ask for linux fusion 360 in forum. No solution presented

## Blog resources

 - ~~[Josh Schertz blog][3]~~ &rarr; uses a paid cloud VM that runs in the browser
 - [blog Rebecca Lucas][7] &rarr; not well discribed guide

## Reddit resources

 - [reddit source 1][5] &rarr; interesting link for troubleshooting
 - [reddit source 2][6] &rarr; discribe workaround wit know issue of check system version
 - [reddit source 3][9] &rarr; looks for the .exe file without a windows machine

## Docker

 - [docker wine][8] &rarr; runs wine in a docker image / use for later stage of the project


[1]: https://appdb.winehq.org/objectManager.php?sClass=version&iId=33392 "wine fusion360 page"
[2]: https://www.autodesk.com/products/fusion-360/students-teachers-educators "fusion 360 product page"
[3]: https://joshschertz.com/2017/08/09/Run-Fusion-360-in-Ubuntu/ "Josh Schertz blog"
[4]: https://forums.autodesk.com/t5/fusion-360-ideastation/build-fusion-for-ubuntu/idi-p/6581544 "forum fusion 360"
[5]: https://www.reddit.com/r/winehq/comments/96wt65/fusion_360_on_wine_with_dxvk/ "reddit source 1"
[6]: https://www.reddit.com/r/Fusion360/comments/a0t7ha/fusion_360_in_wine/ "reddit source 2"
[7]: http://logotim.ru/2018/05/15/fusion-360/#comment-309 "blog Rebecca Lucas"
[8]: https://hub.docker.com/r/scottyhardy/docker-wine/ "docker wine"
[9]: https://www.reddit.com/r/Fusion360/comments/65d0ib/where_is_a_direct_download_link_i_wanna_see_if_i/ "reddit source 3"
[10]: https://appdb.winehq.org/objectManager.php?sClass=application&iId=15617 "second wine link fusion 360"